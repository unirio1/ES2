package com.example.echo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusTranca {

    NOVA("Nova"),
    EM_REPARO("Em reparo"),
    LIVRE("Livre"),
    OCUPADA("Ocupada"),
    APOSENTADA("Aposentada");

    private final String status;

}


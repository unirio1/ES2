package com.example.echo.models;

// Importing required classes

import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Email {

    private String email;
    
    @NotNull(message = "O atributo 'assunto' não pode ser nulo")

    private String assunto;
    
    @NotNull(message = "O atributo 'mensagem' não pode ser nulo")
    private String mensagem;

    private String remetente;
    private String destinatario;

    // public String getDestinatario(){return destinatario;}

    public String getEmail() {
        return email;
    }

    public String getAssunto() {
        return assunto;
    }

    public String getMensagem() {
        return mensagem;
    }
}

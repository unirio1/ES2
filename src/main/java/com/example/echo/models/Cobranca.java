package com.example.echo.models;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Data
@AllArgsConstructor

public class Cobranca {
    private Integer id;
    private String status;
    private LocalDateTime horaSolicitacao;
    private LocalDateTime horaFinalizacao;
    private double valor;
    private Integer ciclista;

    public Cartao getCartao() {
        return getCartao();
    }
}

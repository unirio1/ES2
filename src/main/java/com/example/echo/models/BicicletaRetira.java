package com.example.echo.models;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BicicletaRetira {
    @NotNull(message = "O atributo 'tranca' não pode ser nulo")
    private Integer idTranca;
    @NotNull(message = "O atributo 'bicicleta' não pode ser nulo")
    private Integer idFuncionario;
    @NotNull(message = "O atributo 'totem' não pode ser nulo")
    private Integer idTotem;
}

package com.example.echo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CiclistaBicicleta {
    private Ciclista ciclista;
    private Bicicleta bicicleta;
    private LocalDateTime horaInicio;
    private LocalDateTime horaFim;
    private LocalDateTime horaDevolucao;

    public CiclistaBicicleta(Ciclista ciclista, Bicicleta bicicleta, LocalDateTime horaInicio, LocalDateTime horaFim) {
        this.ciclista = ciclista;
        this.bicicleta = bicicleta;
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
    }
}

package com.example.echo.models;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Passaporte {

    @NotNull(message = "O atributo 'numero' não pode ser nulo")
    String numero;

    @NotNull(message = "O atributo 'validade' não pode ser nulo")
    String validade;

    @NotNull(message = "O atributo 'pais' não pode ser nulo")
    String pais;
}

package com.example.echo.models;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Funcionario {

    private Integer matricula;

    @NotNull(message = "O atributo 'email' não pode ser nulo")
    private String email;

    @NotNull(message = "O atributo 'nome' não pode ser nulo")
    private String nome;

    @NotNull(message = "O atributo 'idade' não pode ser nulo")
    private int idade;

    @NotNull(message = "O atributo 'funcao' não pode ser nulo")
    private String funcao;

    @NotNull(message = "O atributo 'cpf' não pode ser nulo")
    private String cpf;

    @NotNull(message = "O atributo 'senha' não pode ser nulo")
    private String senha;

    @NotNull(message = "O atributo 'confirmacaoSenha' não pode ser nulo")
    private String confirmacaoSenha;
}

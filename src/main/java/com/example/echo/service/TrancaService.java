package com.example.echo.service;

import com.example.echo.enums.StatusBicicleta;
import com.example.echo.enums.StatusTranca;
import com.example.echo.exception.BicicletaStatusNaoOkIncluirException;
import com.example.echo.exception.TrancaNaoDisponivelException;
import com.example.echo.exception.TrancaStatusNaoOkIncluirException;
import com.example.echo.models.*;

import com.example.echo.exception.TrancaNaoEncontradaException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import static com.example.echo.enums.StatusBicicleta.DISPONIVEL;
import static com.example.echo.enums.StatusBicicleta.EM_REPARO;
import static com.example.echo.enums.StatusTranca.*;

@Service
public class TrancaService {
    private final List<Tranca> listaTranca;
    private final List<TrancaTotem> listaTotemsComTrancas = new ArrayList<>();
    private int idNovaTranca=0;
    static String caminhoArquivo = "C:/Users/lucad/OneDrive/Documentos/es2-luca/src/main/java/com/example/equipamento/json/tranca.json";
    static String caminhoArquivoInicial = "C:/Users/lucad/OneDrive/Documentos/es2-luca/src/main/java/com/example/equipamento/json/trancaInicial.json";
    public TrancaService() {
        this.listaTranca = carregarTrancas();
        this.encontrarMaiorId();
    }

    public Tranca cadastrarTranca(Tranca tranca){


        tranca.setIdTranca(idNovaTranca);
        tranca.setStatus(NOVA.getStatus());
        listaTranca.add(tranca);
        idNovaTranca++;
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tranca;
    }
    public List<Tranca> listarTranca() {
        return listaTranca;
    }
    public void salvaTranca() throws IOException {
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaTranca);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }
    private void encontrarMaiorId() {
        int maiorId = 0;
        for (Tranca tranca : this.listaTranca) {
            if (tranca.getIdTranca() > maiorId) {
                maiorId = tranca.getIdTranca();
            }
        }
        // Incrementa o maior ID encontrado para obter o próximo ID disponível
        this.idNovaTranca = maiorId+1;
    }
    public void alterarStatusTranca(Integer idTranca, String status){
        Tranca tranca = encontrarTrancaPorID(idTranca);

        tranca.setStatus(status);
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void incluiTranca(Integer idTranca, Integer numero){
        Tranca tranca = encontrarTrancaPorID(idTranca);

        tranca.setNumero(numero);
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void alterarStatusTranca(Tranca tranca, String status){

        tranca.setStatus(status);
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Tranca> carregarTrancas() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivo);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, Tranca.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public static List<Tranca> cargaInicial(){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivoInicial);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, Tranca.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public Tranca atualizarTranca(Integer idTranca, Tranca trancaAtualizada) {

        Tranca trancaExistente = encontrarTrancaPorID(idTranca);

        trancaExistente.setNumero(trancaAtualizada.getNumero());
        trancaExistente.setLocalizacao(trancaAtualizada.getLocalizacao());
        trancaExistente.setAnoDeFabricacao(trancaAtualizada.getAnoDeFabricacao());
        trancaExistente.setModelo(trancaAtualizada.getModelo());
        trancaExistente.setStatus(trancaAtualizada.getStatus());
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return trancaExistente;
    }
    public Tranca encontrarTrancaPorID(Integer idTranca) {
        for (Tranca tranca : listaTranca){
            if (tranca.getIdTranca()==(idTranca))
                return tranca;
        }
        throw new TrancaNaoEncontradaException();
    }

    public void excluirTranca(Integer idTranca){
        Tranca trancaExistente = encontrarTrancaPorID(idTranca);
        listaTranca.remove(trancaExistente);
    }

    private void salvaTrancaNoTotem() throws IOException {
        String caminhoArquivo = "C:/Users/lucad/OneDrive/Documentos/es2-luca/src/main/java/com/example/equipamento/json/trancaTotem.json";

        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaTotemsComTrancas);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }

    public void incluiTrancaEmTotem(TrancaTotem trancaTotem){

        Totem totemExistente = new TotemService().encontrarTotemPorId(trancaTotem.getIdTotem());
        Tranca trancaExistente = encontrarTrancaPorID(trancaTotem.getIdTranca());
        Funcionario funcionarioExistente = new Funcionario();

        totemExistente.setIdTotem(trancaTotem.getIdTotem());
        funcionarioExistente.setMatricula(trancaTotem.getIdFuncionario());
        trancaExistente.setIdTranca(trancaTotem.getIdTranca());




        incluiTranca(trancaTotem.getIdTranca(), trancaExistente.getNumero());


        if (!trancaExistente.getStatus().equals(StatusTranca.NOVA.getStatus()) && !trancaExistente.getStatus().equals(EM_REPARO.getStatus())){
            throw new TrancaStatusNaoOkIncluirException();
        }
        this.alterarStatusTranca(trancaExistente, LIVRE.getStatus());
        listaTotemsComTrancas.add(trancaTotem);
        try {
            salvaTrancaNoTotem();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}



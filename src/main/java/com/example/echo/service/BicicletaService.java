package com.example.echo.service;

import com.example.echo.exception.*;
import com.example.echo.models.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.example.echo.enums.StatusBicicleta.*;
import static com.example.echo.enums.StatusTranca.LIVRE;
import static com.example.echo.enums.StatusTranca.OCUPADA;


@Service
public class BicicletaService {

    private final List<Bicicleta> listaBicicleta;
    private final List<BicicletaTranca> listaTrancasComBicicletas;
    private int idNovaBicicleta=0;
    static String caminhoArquivo = "C:/Users/lucad/OneDrive/Documentos/es2-luca/src/main/java/com/example/equipamento/json/bicicleta.json";
    static String caminhoArquivoBicicletaTranca = "C:/Users/lucad/OneDrive/Documentos/es2-luca/src/main/java/com/example/equipamento/json/bicicletaTranca.json";
    public BicicletaService() {
        this.listaTrancasComBicicletas = carregarBicicletasNaTranca();
        this.listaBicicleta = carregarBicicletas();
        this.encontrarMaiorId();
    }
    public Bicicleta cadastrarBicicleta(Bicicleta bicicleta){

        bicicleta.setIdBicicleta(idNovaBicicleta);
        bicicleta.setStatus(NOVA.getStatus());
        listaBicicleta.add(bicicleta);
        idNovaBicicleta++;
        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bicicleta;
    }
    public List<Bicicleta> listarBicicleta() {
        return listaBicicleta;
    }
    public List<BicicletaTranca> listarBicicletaNaTranca() {
        return listaTrancasComBicicletas;
    }
    public void salvaBicicleta() throws IOException {
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaBicicleta);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }

    private void encontrarMaiorId() {
        int maiorId = 0;
        for (Bicicleta bicicleta : this.listaBicicleta) {
            if (bicicleta.getIdBicicleta() > maiorId) {
                maiorId = bicicleta.getIdBicicleta();
            }
        }

        this.idNovaBicicleta = maiorId+1;
    }
    public Bicicleta atualizarBicicleta(int idBicicleta, Bicicleta bicicletaAtualizada) {

        Bicicleta bicicletaExistente = encontrarBicicletaPorID(idBicicleta);

        bicicletaExistente.setMarca(bicicletaAtualizada.getMarca());
        bicicletaExistente.setModelo(bicicletaAtualizada.getModelo());
        bicicletaExistente.setAno(bicicletaAtualizada.getAno());
        bicicletaExistente.setNumero(bicicletaAtualizada.getNumero());
        bicicletaExistente.setStatus(bicicletaAtualizada.getStatus());

        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return bicicletaExistente;
    }

    public static List<Bicicleta> carregarBicicletas() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivo);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, Bicicleta.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }
    public static List<BicicletaTranca> carregarBicicletasNaTranca() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivoBicicletaTranca);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, BicicletaTranca.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }
    public Bicicleta encontrarBicicletaPorID(int idBicicleta) {
        for (Bicicleta bicicleta : listaBicicleta){
            if (bicicleta.getIdBicicleta()==(idBicicleta))
                return bicicleta;
        }
        throw new BicicletaNaoEncontradaException();
    }
    public void excluirBicicleta(int idBicicleta){
        Bicicleta bicicletaExistente = encontrarBicicletaPorID(idBicicleta);
        listaBicicleta.remove(bicicletaExistente);
    }

    private void salvaBicicletaNaTranca() throws IOException {
        String caminhoArquivo = "C:/Users/lucad/OneDrive/Documentos/es2-luca/src/main/java/com/example/equipamento/json/bicicletaTranca.json";

        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaTrancasComBicicletas);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }

    public void incluiBicicletaEmTranca(BicicletaTranca bicicletaTranca){

        Tranca trancaExistente = new TrancaService().encontrarTrancaPorID(bicicletaTranca.getIdTranca());
        Bicicleta bicicletaExistente = encontrarBicicletaPorID(bicicletaTranca.getIdBicicleta());
        Funcionario funcionarioExistente = new Funcionario();

        trancaExistente.setBicicleta(bicicletaTranca.getIdBicicleta());
        funcionarioExistente.setMatricula(bicicletaTranca.getIdFuncionario());
        trancaExistente.setIdTranca(bicicletaTranca.getIdTranca());

        listaTrancasComBicicletas.add(bicicletaTranca);


        if(!trancaExistente.getStatus().equals(LIVRE.getStatus())){
            throw new TrancaNaoDisponivelException();

        }
        if (!bicicletaExistente.getStatus().equals(NOVA.getStatus()) && !bicicletaExistente.getStatus().equals(EM_REPARO.getStatus())){
            throw new BicicletaStatusNaoOkIncluirException();
        }
        new TrancaService().alterarStatusTranca(bicicletaTranca.getIdTranca(), OCUPADA.getStatus());
        this.alterarStatusBicicleta(bicicletaExistente, DISPONIVEL.getStatus());

        try {
            salvaBicicletaNaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    public void retirarBicicletaDaTranca(BicicletaRetira bicicletaRetira){

        Tranca trancaExistente = new TrancaService().encontrarTrancaPorID(bicicletaRetira.getIdTranca());
        Funcionario funcionarioExistente = new Funcionario();
        Totem totemExistente = new TotemService().encontrarTotemPorId(bicicletaRetira.getIdTotem());

        BicicletaTranca bicicletaExistenteNaTranca = encontrarBicicletaPorIDNaTranca(bicicletaRetira.getIdTranca());
        Bicicleta bicicletaExistente = encontrarBicicletaPorID(bicicletaExistenteNaTranca.getIdBicicleta());

        totemExistente.setIdTotem(bicicletaRetira.getIdTotem());
        funcionarioExistente.setMatricula(bicicletaRetira.getIdFuncionario());
        trancaExistente.setIdTranca(bicicletaRetira.getIdTranca());

        listaTrancasComBicicletas.remove(bicicletaRetira);


        if(!trancaExistente.getStatus().equals(OCUPADA.getStatus())){
            throw new TrancaStatusNaoOcupada();

        }
        if (!bicicletaExistente.getStatus().equals(REPARO_SOLICITADO.getStatus())){
            throw new BicicletaStatusNaoOkRetirarException();
        }
        new TrancaService().alterarStatusTranca(bicicletaRetira.getIdTranca(), DISPONIVEL.getStatus());
        this.alterarStatusBicicleta(bicicletaExistente, EM_REPARO.getStatus());

        try {
            salvaBicicletaNaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public Bicicleta alterarStatusBicicleta(Integer idBicicleta, String status){
        Bicicleta bicicleta = encontrarBicicletaPorID(idBicicleta);

        bicicleta.setStatus(status);
        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bicicleta;
    }
    public void alterarStatusBicicleta(Bicicleta bicicleta, String status){

        bicicleta.setStatus(status);
        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public BicicletaTranca buscarBicicletaComIdTranca(Integer idTranca){

        for (BicicletaTranca bicicletaTranca : listaTrancasComBicicletas){
            if (bicicletaTranca.getIdTranca().equals(idTranca))
                return bicicletaTranca;
        }
        throw new TrancaNaoEncontradaException();
    }
    public Bicicleta mostraBicicletaNaTranca(Integer idTranca){

        BicicletaTranca bicicletaTranca = buscarBicicletaComIdTranca(idTranca);
        return encontrarBicicletaPorID(bicicletaTranca.getIdBicicleta());
    }

    public BicicletaTranca encontrarBicicletaPorIDNaTranca(Integer idBicicleta) {
        for (BicicletaTranca bicicletaTranca : listaTrancasComBicicletas){
            if (bicicletaTranca.getIdBicicleta().equals(idBicicleta))
                return bicicletaTranca;
        }
        throw new BicicletaNaoEncontradaException();
    }

}





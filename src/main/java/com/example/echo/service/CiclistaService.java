package com.example.echo.service;

import com.example.echo.dto.requests.AluguelRequest;
import com.example.echo.dto.requests.CadastroCiclista;
import com.example.echo.dto.requests.DevolucaoRequest;
import com.example.echo.dto.responses.AluguelResponse;
import com.example.echo.dto.responses.DevolucaoResponse;
import com.example.echo.exception.*;
import com.example.echo.models.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.echo.enums.StatusCiclistaEnum.ATIVO;
import static com.example.echo.enums.StatusCiclistaEnum.INATIVO;
import static com.example.echo.enums.StatusTranca.*;
import static com.example.echo.enums.StatusBicicleta.*;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class CiclistaService {

    @Autowired
    private ExternoService externoService;

    @Autowired
    private BicicletaService bicicletaService;

    @Autowired
    private TrancaService trancaService;

    @Autowired
    private EquipamentoService equipamentoService;

    private final List<Ciclista> listaCiclistas = new ArrayList<>();
    private final List<Cartao> listaCartoes = new ArrayList<>();
    private final List<CiclistaBicicleta> listaCiclistaComBicicletas = new ArrayList<>();
    private int idNovoCiclista = 1;

    private static final String SUCESSO = "SUCESSO";
    private static final Integer COBRANCA = 10;

    public String aloMundo(){
        return "Alo, mundo";
    }

    public Ciclista cadastrarCiclista(CadastroCiclista cadastroCiclista){

        if (existeEmailCiclista(cadastroCiclista.getCiclista().getEmail()))
            throw new EmailJaCadastradoException();

        cadastroCiclista.getCiclista().setId(idNovoCiclista);
        cadastroCiclista.getCartao().setIdCiclista(idNovoCiclista);
        cadastroCiclista.getCiclista().setStatus(INATIVO.getStatus());

        externoService.validaCartaoDeCredito(cadastroCiclista.getCartao());

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        externoService.enviarEmail(email);

        listaCiclistas.add(cadastroCiclista.getCiclista());
        listaCartoes.add(cadastroCiclista.getCartao());

        idNovoCiclista++;

        return cadastroCiclista.getCiclista();
    }

    public Ciclista encontrarCiclistaPorId(Integer idCiclista) {
        for (Ciclista ciclista : listaCiclistas){
            if (ciclista.getId().equals(idCiclista))
                return ciclista;
        }
        throw new CiclistaNaoEncontradoException();
    }

    public Ciclista atualizarCiclista(Integer idCiclista, Ciclista ciclistaAtualizado) {

        Ciclista ciclistaExistente = encontrarCiclistaPorId(idCiclista);

        ciclistaExistente.setEmail(ciclistaAtualizado.getEmail());
        ciclistaExistente.setNome(ciclistaAtualizado.getNome());
        ciclistaExistente.setNascimento(ciclistaAtualizado.getNascimento());
        ciclistaExistente.setNacionalidade(ciclistaAtualizado.getNacionalidade());
        ciclistaExistente.setCpf(ciclistaAtualizado.getCpf());
        ciclistaExistente.setSenha(ciclistaAtualizado.getSenha());
        ciclistaExistente.setPassaporte(ciclistaAtualizado.getPassaporte());
        ciclistaExistente.setUrlFotoDocumento(ciclistaAtualizado.getUrlFotoDocumento());

        Email email = new Email(ciclistaExistente.getEmail(), "Dados atualizados!", "Seus dados foram atualizados com sucesso!", null, null);

        externoService.enviarEmail(email);

//        if (!externoService.enviarEmail().equals(SUCESSO)) {
//            throw new ErroEnvioEmailException();
//        }

        return ciclistaExistente;
    }

    public Boolean existeEmailCiclista(String email){
        for (Ciclista ciclista : listaCiclistas){
            if (ciclista.getEmail().equals(email))
                return true;
        }
        return false;
    }

    public Ciclista ativarEmail(Integer idCiclista){

        Ciclista ciclista = encontrarCiclistaPorId(idCiclista);
        ciclista.setStatus(ATIVO.getStatus());
        return ciclista;
    }

    public Boolean permiteAluguel(Integer idCiclista) {
        Ciclista ciclista = encontrarCiclistaPorId(idCiclista);

        if (!ciclista.getStatus().equals(ATIVO.getStatus()))
            return false;

        for (CiclistaBicicleta ciclistaBicicleta : listaCiclistaComBicicletas){
            if (ciclistaBicicleta.getCiclista().getId().equals(idCiclista))
                return false;
        }
        return true;
    }

    public Bicicleta encontraBicicletaAlugadaPorIdCiclista(Integer idCiclista) {
        encontrarCiclistaPorId(idCiclista);

        for (CiclistaBicicleta ciclistaBicicleta : listaCiclistaComBicicletas){
            if (ciclistaBicicleta.getCiclista().getId().equals(idCiclista))
                return ciclistaBicicleta.getBicicleta();
        }
        return null;
    }

    public Cartao encontrarMeioDePagamento(Integer idCiclista) {
        for (Cartao cartao : listaCartoes){
            if (cartao.getIdCiclista().equals(idCiclista))
                return cartao;
        }
        throw new CartaoNaoEncontradoException();
    }

    public Cartao atualizarMeioDePagamento(Integer idCiclista, Cartao cartaoAtualizado) {

        Cartao cartaoExistente = encontrarMeioDePagamento(idCiclista);
        cartaoExistente.setCvv(cartaoAtualizado.getCvv());
        cartaoExistente.setNumero(cartaoAtualizado.getNumero());
        cartaoExistente.setNomeTitular(cartaoAtualizado.getNomeTitular());
        cartaoExistente.setValidade(cartaoAtualizado.getValidade());

        return cartaoExistente;
    }

    public AluguelResponse realizarAluguel(AluguelRequest aluguelRequest) {
        Ciclista ciclista = encontrarCiclistaPorId(aluguelRequest.getCiclista());

        if (!permiteAluguel(aluguelRequest.getCiclista()))
            throw new AluguelNaoPermitidoException();

//        if (!externoService.enviarCobranca().equals(SUCESSO))
//            throw new ErroEnviarCobrancaException();

        Bicicleta bicicleta =  bicicletaService.mostraBicicletaNaTranca(aluguelRequest.getTrancaInicio());

//        Bicicleta bicicleta = new Bicicleta(1, "MarcaBike", "Teste", "2022", 131, "Em uso");

        CiclistaBicicleta ciclistaBicicleta = new CiclistaBicicleta(ciclista, bicicleta, LocalDateTime.now(), LocalDateTime.now().plusHours(2));

        listaCiclistaComBicicletas.add(ciclistaBicicleta);

//        if (!equipamentoService.alteraStatusTranca().equals(SUCESSO))
//            throw new ErroAlterarStatusTrancaException();

        Email email = new Email(ciclista.getEmail(), "Bicicleta alugada!",
                "Você alugou uma bicicleta com sucesso! Horário: " + LocalDateTime.now() + " , e o valor cobrado foi: " + COBRANCA,
                null, null);

        externoService.enviarEmail(email);

        Cobranca cobranca = new Cobranca(null, "pendente", null, LocalDateTime.now().plusHours(2),
                COBRANCA, ciclistaBicicleta.getCiclista().getId());

        externoService.fazerCobranca(cobranca);

        bicicletaService.alterarStatusBicicleta(bicicleta.getIdBicicleta(), EM_USO.getStatus());

        trancaService.alterarStatusTranca(aluguelRequest.getTrancaInicio(), LIVRE.getStatus());

        return new AluguelResponse(ciclistaBicicleta.getBicicleta().getIdBicicleta(), ciclistaBicicleta.getHoraInicio().toString(), 34,
                ciclistaBicicleta.getHoraFim().toString(), COBRANCA, aluguelRequest.getCiclista(), aluguelRequest.getTrancaInicio());
    }

    public DevolucaoResponse realizarDevolucao(DevolucaoRequest devolucaoRequest) {

        if (!equipamentoService.validaNumeroBicicleta().equals(SUCESSO))
            throw new NumeroBicicletaInvalidoException();

        CiclistaBicicleta ciclistaBicicleta = encontraCiclistaBicicletaPorIdBicicleta(devolucaoRequest.getIdBicicleta());
        ciclistaBicicleta.setHoraDevolucao(LocalDateTime.now());

        long duracaoMinutos = Duration.between(ciclistaBicicleta.getHoraInicio(), ciclistaBicicleta.getHoraDevolucao()).toMinutes();
        double horas = (double) duracaoMinutos / 60;
        long minutosExtras = duracaoMinutos - 120;

        double cobrancaExtra = 0;

        if (horas > 2)
            cobrancaExtra = (Math.floorDiv(minutosExtras, 30) * 5);

        Email email = new Email(ciclistaBicicleta.getCiclista().getEmail(), "Bicicleta devolvida!", "Você devolveu sua bicicleta!", null, null);

        externoService.enviarEmail(email);

        Cobranca cobranca = new Cobranca(null, "pendente", LocalDateTime.now(), LocalDateTime.now().plusHours(2),
                cobrancaExtra, ciclistaBicicleta.getCiclista().getId());

        externoService.incluirCobranca(cobranca);

        bicicletaService.alterarStatusBicicleta(devolucaoRequest.getIdBicicleta(), DISPONIVEL.getStatus());

        trancaService.alterarStatusTranca(devolucaoRequest.getIdTranca(), OCUPADA.getStatus());

        listaCiclistaComBicicletas.remove(ciclistaBicicleta);

        return new DevolucaoResponse(ciclistaBicicleta.getBicicleta().getIdBicicleta(), ciclistaBicicleta.getHoraInicio().toString(), devolucaoRequest.getIdTranca(),
                ciclistaBicicleta.getHoraDevolucao().toString(), cobrancaExtra, ciclistaBicicleta.getCiclista().getId());
    }

    public CiclistaBicicleta encontraCiclistaBicicletaPorIdBicicleta(Integer idBicicleta) {
        for (CiclistaBicicleta ciclistaBicicleta : listaCiclistaComBicicletas){
            if (ciclistaBicicleta.getBicicleta().getIdBicicleta() == idBicicleta)
                return ciclistaBicicleta;
        }
        return null;
    }
}

package com.example.echo.service;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
public class EchoService {

    public String aloMundo(){
        return "Alo, mundo";
    }
}

package com.example.echo.service;

import com.example.echo.exception.*;
import com.example.echo.models.Cartao;
import com.example.echo.models.Cobranca;
import com.example.echo.models.Email;
import com.example.echo.models.ErrorResponse;
import jakarta.validation.constraints.NotNull;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class ExternoService {

    public String enviarEmail(){
        return "SUCESSO";
    }

    public String enviarCobranca(){
        return "SUCESSO";
    }

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}") private String sender;
    private final List<Cobranca> listaCobranca = new ArrayList<>();
    private static final Logger logger = LoggerFactory.getLogger(ExternoService.class);
    private LocalDateTime ultimaVerificacao = LocalDateTime.now();

    // --- EMAIL ---
    public ResponseEntity<Object> enviarEmail(@NotNull Email details) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(sender);
        mailMessage.setTo(details.getEmail());
        mailMessage.setSubject(details.getAssunto());
        mailMessage.setText(details.getMensagem());

        try {
            javaMailSender.send(mailMessage);
            return ResponseEntity.ok("enviado com sucesso");
        } catch (Exception ex) {
            // Verifica se o erro é relacionado ao formato do e-mail
            if (ex.getMessage().contains("Invalid Addresses")) {
                //ErrorResponse erroResponse = new ErrorResponse("422", "Formato de e-mail inválido");
                //return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(erroResponse);
                throw new EmailFormatoInvalidoException();
            }
            // Verifica se o erro é relacionado ao e-mail não existir
            else if (ex.getMessage().contains("Recipient address rejected")) {
//                ErrorResponse erroResponse = new ErrorResponse("404", "E-mail não encontrado");
//                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erroResponse);
                throw new EmailNaoExisteException();
            }
            // Outro tipo de erro
            else {
                //return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro 500");
                throw new ErroEnvioEmailException();
            }
        }

    }

    //---------COBRANÇA----------

    //registrar uma nova cobrança pegando o horário atual do pc
    //garantir que cada cobrança tenha um ID único (lista de cobrança + 1)

    public ResponseEntity<Object> fazerCobranca(@NotNull Cobranca fazCobranca) {
        // Adicione a lógica para verificar se os dados da cobrança são inválidos
        if (fazCobranca.getValor() <= 0.0 || fazCobranca.getCiclista() == null) {
            ErrorResponse erroResponse = new ErrorResponse("422", "Dados da cobrança inválidos");
            return ResponseEntity.status(422).body(erroResponse);
        }

        fazCobranca.setId(listaCobranca.size() + 1);
        fazCobranca.setHoraSolicitacao(LocalDateTime.now());

        listaCobranca.add(fazCobranca);

        return ResponseEntity.status(200).body(fazCobranca);
    }

    //buscar a cobrança na lista pelo ID
    //ter cuidado quando o retorno for nulo (criar exceção)
    public Cobranca obterCobrancaID(Integer idCobranca) {
        Optional<Cobranca> cobrancaOptional = listaCobranca.stream()
                .filter(cobranca -> cobranca.getId().equals(idCobranca)) //filtra a lista usando o id
                .findFirst();

        return cobrancaOptional.orElse(null);
    }

    public Cobranca incluirCobranca(@NotNull Cobranca incluiCobranca) {
        incluiCobranca.setId(listaCobranca.size() + 1);
        //incluiCobranca.setHoraSolicitadacao(LocalDateTime.now());

        if (incluiCobranca.getValor() == 0.0 || incluiCobranca.getCiclista() == null) {
            throw new RuntimeException("Os campos valor e ciclista são obrigatorios");
        }
        listaCobranca.add(incluiCobranca);
        return incluiCobranca;
    }

    public ResponseEntity<String> processarFila() {
        // Verifica se passaram 12 horas desde a última verificação
        if (verificarIntervaloDeTempo()) {
            List<Cobranca> cobrancasProcessadas = new ArrayList<>();

            // Passo 1: Identificar cobranças relativas a taxas extras de aluguel não cobradas
            for (Cobranca cobranca : listaCobranca) {
                if (cobranca.getStatus().equals("pendente")) {
                    cobranca.setStatus("processada");
                    cobranca.setHoraSolicitacao(LocalDateTime.now());
                    cobrancasProcessadas.add(cobranca);
                }
            }

            // Passo 2: Realizar a cobrança para cada ciclista em atraso
            for (Cobranca cobranca : cobrancasProcessadas) {
                try {
                    Cartao cartao = obterCartaoMaisRecente(cobranca.getCiclista());
                    realizarCobranca(cobranca, cartao);

                    // Passo 3: Notificar ciclista sobre a cobrança em atraso
                    notificarCiclista(cobranca);

                } catch (Exception e) {
                    // [E1] Erro no pagamento ou pagamento não autorizado
                    // A2.1: Manter o valor da cobrança extra para efetuar posteriormente
                    cobranca.setStatus("pendente");
                    // A2.1: Volta para o passo 2 para o próximo ciclista
                    continue;
                }
                // Regra R1 – Devem ser registrados: data/hora da cobrança, valor extra do aluguel, o cartão usado para cobrança
                registrarDadosCobranca(cobranca);
            }

            // Remover cobranças processadas da lista
            listaCobranca.removeAll(cobrancasProcessadas);

            if (!cobrancasProcessadas.isEmpty()) {
                return ResponseEntity.status(200).body("Cobranças processadas com sucesso.");
            }

            return ResponseEntity.status(200).body("Nenhuma cobrança atrasada na fila.");
        } else {
            return ResponseEntity.status(200).body("Não passaram 12 horas desde a última verificação.");
        }
    }

    private boolean verificarIntervaloDeTempo() {
        LocalDateTime agora = LocalDateTime.now();
        return agora.minusHours(12).isAfter(ultimaVerificacao);
    }

    private void registrarDadosCobranca(Cobranca cobranca) {
        logger.info("Cobrança registrada - ID: {}, Valor: {}, Cartão: {}, Data/Hora: {}",
                cobranca.getId(), cobranca.getValor(), cobranca.getCartao().getNumero(),
                cobranca.getHoraSolicitacao());
    }

    private Cartao obterCartaoMaisRecente(Integer ciclistaId) {
        return new Cartao();
    }

    private void realizarCobranca(Cobranca cobranca, Cartao cartao) {
        //tentativa
        logger.info("Cobrança realizada - ID: {}, Valor: {}, Cartão: {}, Data/Hora: {}",
                cobranca.getId(), cobranca.getValor(), cartao.getNumero(),
                cobranca.getHoraSolicitacao());
    }

    private void notificarCiclista(Cobranca cobranca) {

        // Recupera os detalhes necessários para enviar o e-mail (endereço, assunto, mensagem)
        String emailDestinatario = obterEmailCiclista(cobranca.getCiclista());
        String assunto = "Notificação de Cobrança em Atraso";
        String mensagem = construirMensagemNotificacao(cobranca);

        Email email = new Email();

        // Envia o e-mail usando o método enviarEmail
        try {
            ResponseEntity<Object> resultado = enviarEmail(email);

            if (resultado.getStatusCode().is2xxSuccessful()) {
                logger.info("Notificação enviada ao ciclista - ID: {}, Valor: {}, Data/Hora: {}",
                        cobranca.getId(), cobranca.getValor(), cobranca.getHoraSolicitacao());
            } else {
                logger.error("Falha ao enviar notificação ao ciclista - ID: {}, Valor: {}, Data/Hora: {}",
                        cobranca.getId(), cobranca.getValor(), cobranca.getHoraSolicitacao());
            }
        } catch (Exception e) {
            logger.error("Erro ao enviar notificação ao ciclista - ID: {}, Valor: {}, Data/Hora: {}",
                    cobranca.getId(), cobranca.getValor(), cobranca.getHoraSolicitacao(), e);
        }
    }

    private String obterEmailCiclista(Integer ciclistaId) {
        //teste
        return "ciclista@example.com";
    }

    private String construirMensagemNotificacao(Cobranca cobranca) {

        return "Prezado ciclista, a cobrança referente ao aluguel está em atraso. Valor: " +
                cobranca.getValor() + " - Data/Hora: " + cobranca.getHoraSolicitacao();
    }


    //----------CARTAO-------
    //Atualizei o cod de clara
    public Cartao validaCartaoDeCredito(@NotNull Cartao cartao) {
        // Verifica se todos os campos necessários estão preenchidos
        if (cartao.getNumero() == null || cartao.getNomeTitular() == null || cartao.getValidade() == null || cartao.getCvv() == null) {
            throw new RuntimeException("Todos os campos do cartão de crédito são obrigatórios");
        }

        // Verifica se o número do cartão tem 16 dígitos
        if (cartao.getNumero().length() != 16) {
            throw new CartaoCreditoInvalidoException();
        }

        // Verifica se a data de validade é válida
        if (!isValidDataValidade(cartao.getValidade())) {
            throw new DataValidadeCCInvalidaException();
        }
        return cartao;
    }


    //Clara que implementou esse
    private boolean isValidDataValidade(String dataValidade) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            YearMonth dataValidadeFormatada = YearMonth.parse(dataValidade, formatter);

            // Verifica se a data é igual ou posterior à data atual
            return !dataValidadeFormatada.isBefore(YearMonth.now());
        } catch (Exception e) {
            return false;
        }
    }
}

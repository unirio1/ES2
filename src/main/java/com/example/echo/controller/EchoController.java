package com.example.echo.controller;

import com.example.echo.service.EchoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.echo.webservice.SwapiClient;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;

/**
 * API Echo - Projeto base para a disciplina de Programação Modular.
 *
 * Os métodos da API Echo são documentados utilizando a especificação OpenAPI (Swagger) e podem ser visualizados
 * acessando a URL do domínio + "/swagger-ui.html".
 * Acesso em servidor local: http://localhost:8080/swagger-ui.html
 */

@RestController
@RequestMapping("/echo")
//@Api(value="Spring Boot REST API Echo")
public class EchoController {

	EchoService echoService;

	EchoController(EchoService echoService){
		this.echoService = echoService;
	}

	@Autowired
	private SwapiClient swapi;

	@GetMapping("/{palavra}")
//	@ApiOperation(value="Retorna repetição da palavra informada")
	public String getEcho (@PathVariable (value ="palavra") String palavra){
		return palavra +" "+ palavra +" "+ palavra;
	}

	@GetMapping("/alo-mundo")
//	@ApiOperation(value="Retorna um 'Alo, mundo'")
	public String getAloMundo () {
		return echoService.aloMundo();
	}


}
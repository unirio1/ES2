package com.example.echo.controller;

import com.example.echo.exception.ErroException;
import com.example.echo.models.Bicicleta;
import com.example.echo.models.Tranca;
import com.example.echo.models.TrancaTotem;
import com.example.echo.service.BicicletaService;
import com.example.echo.service.TrancaService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class TrancaController {
    public final TrancaService trancaService;
    public final BicicletaService bicicletaService;
    @PostMapping("/tranca")
    public Tranca cadastrarTranca( @RequestBody Tranca tranca) {
        return trancaService.cadastrarTranca(tranca);
    }
    @GetMapping("/tranca")
    public List<Tranca> listarTranca(){
        return trancaService.listarTranca();
    }

    @PutMapping("/tranca/{idTranca}")
    public Tranca atualizarTranca(@PathVariable int idTranca, @RequestBody Tranca trancatualizada) {
        return trancaService.atualizarTranca(idTranca, trancatualizada);
    }
    @DeleteMapping("/tranca/{idTranca}")
    public void excluirTranca(@PathVariable Integer idTranca) {
        trancaService.excluirTranca(idTranca);
    }
    @PostMapping("/tranca/integrarNaRede")
    public ResponseEntity<?> incluiBicicletaEmTotem(@Valid @RequestBody TrancaTotem trancaTotem){
        try {
            trancaService.incluiTrancaEmTotem(trancaTotem);
            return ResponseEntity.status(HttpStatus.OK).body("Dados cadastrados");
        }catch (RuntimeException ex){
            ErroException[] erros = new ErroException[1];
            erros[0] = new ErroException("422", ex.getMessage());

            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(erros);
        }
    }
    @GetMapping("/tranca/{idTranca}/bicicleta")
    public Bicicleta buscarBicicletaComIdTranca(@PathVariable Integer idTranca){
        return bicicletaService.mostraBicicletaNaTranca(idTranca);
    }


}



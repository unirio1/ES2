package com.example.echo.controller;

import com.example.echo.models.Totem;
import com.example.echo.service.TotemService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor

public class TotemController {

    private final TotemService totemService;


    @GetMapping("/totem")
    public List<Totem> listarTotens() {
        return totemService.listarTotens();
    }

    @PostMapping("/totem")
    public Totem cadastrarTotem(@RequestBody Totem totem) {
        return totemService.cadastraTotem(totem);
    }

    @PutMapping("/totem/{idTotem}")
    public Totem atualizarTotem(@PathVariable int idTotem, @Valid @RequestBody Totem totemAtualizado) {
        return totemService.atualizarTotem(idTotem, totemAtualizado);
    }

    @DeleteMapping("/totem/{idTotem}")
    public void excluirTotem(@PathVariable Integer idTotem) {
        totemService.excluirTotem(idTotem);
    }
}
//    @GetMapping("/totem/{idTotem}/trancas")
//    private List<Tranca> listarTrancas(){
//        return TotemService.listarTrancas();
//    }
//    @GetMapping("/totem/{idTotem}/bicicletas")
//    private List<Bicicleta> listarBicicletas(){
//        return TotemService.listarBicicletas();
//    }
//}



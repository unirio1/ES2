package com.example.echo.controller;

import com.example.echo.exception.ErroException;
import com.example.echo.models.Bicicleta;
import com.example.echo.models.BicicletaRetira;
import com.example.echo.models.BicicletaTranca;
import com.example.echo.service.BicicletaService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
public class BicicletaController {
    private final BicicletaService bicicletaService;
    @PostMapping("/bicicleta")
    public Bicicleta cadastrarBicicleta(@RequestBody Bicicleta bicicleta) {
        return bicicletaService.cadastrarBicicleta(bicicleta);
    }
    @GetMapping("/bicicleta")
    public List<Bicicleta> listarBicicletas(){
        return bicicletaService.listarBicicleta();
    }

    @PutMapping("/bicicleta/{idBicicleta}")
    public Bicicleta atualizarBicicleta(@PathVariable Integer idBicicleta, @Valid @RequestBody Bicicleta bicicletaAtualizada) {
        return bicicletaService.atualizarBicicleta(idBicicleta, bicicletaAtualizada);
    }
    @DeleteMapping("/bicicleta/{idBicicleta}")
    public void excluirBicicleta(@PathVariable Integer idBicicleta) {
        bicicletaService.excluirBicicleta(idBicicleta);
    }

    @PostMapping("/bicicleta/integrarNaRede")
    public ResponseEntity<?> incluiBicicletaEmTotem(@Valid@RequestBody BicicletaTranca bicicletaTranca){
        try {
            bicicletaService.incluiBicicletaEmTranca(bicicletaTranca);
            return ResponseEntity.status(HttpStatus.OK).body("Dados cadastrados");
        }catch (RuntimeException ex){
            ErroException[] erros = new ErroException[1];
            erros[0] = new ErroException("422", ex.getMessage());

            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(erros);
        }
    }

    @PostMapping("/bicicleta/{idBicicleta}/status/{status}")
    public Bicicleta alterarStatusBicicleta(@PathVariable Integer idBicicleta,String status) {
        return bicicletaService.alterarStatusBicicleta(idBicicleta,status);
    }
    @PostMapping("/bicicleta/retirarDaRede")
    public ResponseEntity<?> retirarBicicletaDaTranca(@Valid@RequestBody BicicletaRetira bicicletaRetira){
        try {
            bicicletaService.retirarBicicletaDaTranca(bicicletaRetira);
            return ResponseEntity.status(HttpStatus.OK).body("Dados cadastrados");
        }catch (RuntimeException ex){
            ErroException[] erros = new ErroException[1];
            erros[0] = new ErroException("422", ex.getMessage());

            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(erros);
        }
    }

}




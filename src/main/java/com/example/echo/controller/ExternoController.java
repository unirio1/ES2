package com.example.echo.controller;

import com.example.echo.models.Cartao;
import com.example.echo.models.Cobranca;
import com.example.echo.models.Email;
import com.example.echo.service.ExternoService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
public class ExternoController {

    private final ExternoService externoService;

    //--------endpoints--------
    //post para enviar email (enviarEmail)
    //criar uma classe para criar email
    @PostMapping("/enviarEmail")
    public ResponseEntity<Object> enviarEmail(@RequestBody Email email ){
        try{
            return externoService.enviarEmail(email);
        }catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email não encontrado");
        }
    }

    //post para fazer a cobrança (cobranca)
    @PostMapping("/cobranca")
    public Object fazerCobranca(@RequestBody Cobranca fazCobranca) {
        return externoService.fazerCobranca(fazCobranca);
    }

    @PostMapping("/processaCobrancaEmFila")
    public Object processarFila(@RequestBody Cobranca processaFila){
        try{
            return externoService.processarFila();
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao processar a fila de cobranças.");
        }
    }

    //get para obter a cobrança (cobranca/{idCobranca})
    @GetMapping("/cobranca/{idCobranca}")
    public Cobranca obterCobrancaID(@PathVariable Integer idCobranca) {
        return externoService.obterCobrancaID(idCobranca);
    }

    //inclui cobrança na fila
    @PostMapping("/filaCobranca")
    public Cobranca incluirCobranca(@RequestBody Cobranca incluiCobranca){
        return externoService.incluirCobranca(incluiCobranca);
    }

    //post para validar cartão de crédito (validaCartaoDeCredito)
    // LEMBRAR de trocar boolean para Cartao
    //retornando como tipo Cartão está quebrando o codigo, consertar depois
    @PostMapping("/validaCartaoDeCredito")
    public Cartao validaCartaoDeCredito(@RequestBody Cartao cartao) {
        return externoService.validaCartaoDeCredito(cartao);
    }
}

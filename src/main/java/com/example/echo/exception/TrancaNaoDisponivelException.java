package com.example.echo.exception;

public class TrancaNaoDisponivelException extends RuntimeException{
    public TrancaNaoDisponivelException(){super("Erro: Tranca não está disponível");}
}


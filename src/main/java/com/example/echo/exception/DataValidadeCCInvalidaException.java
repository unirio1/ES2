package com.example.echo.exception;

public class DataValidadeCCInvalidaException extends RuntimeException{
    public DataValidadeCCInvalidaException() {
        super("Erro: Data de validade do cartão de crédito inválida");
    }
}

package com.example.echo.exception;

public class TrancaStatusNaoOcupada extends RuntimeException{
    public TrancaStatusNaoOcupada(){super("Erro: Status da tranca não é igual a ocupada");}
}


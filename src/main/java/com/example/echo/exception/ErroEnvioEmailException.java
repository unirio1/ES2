package com.example.echo.exception;

public class ErroEnvioEmailException extends RuntimeException{
    public ErroEnvioEmailException() {
        super("Erro: Erro no envio de Email");
    }
}

package com.example.echo.exception;

public class CartaoCreditoInvalidoException extends RuntimeException{
    public CartaoCreditoInvalidoException() {
        super("Erro: Número de cartão de crédito inválido");
    }
}

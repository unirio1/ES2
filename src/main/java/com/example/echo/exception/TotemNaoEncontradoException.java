package com.example.echo.exception;

public class TotemNaoEncontradoException extends RuntimeException{
    public TotemNaoEncontradoException() {
        super("Erro: Totem não encontrado");
    }
}


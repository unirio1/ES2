package com.example.echo.exception;

public class TrancaNaoEncontradaException extends RuntimeException{
    public TrancaNaoEncontradaException(){super("Erro: Tranca não encontrada");}
}


package com.example.echo.exception;

public class NumeroBicicletaInvalidoException extends RuntimeException{
    public NumeroBicicletaInvalidoException() {
        super("Erro: Numero de bicicleta invalido");
    }
}

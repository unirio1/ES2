package com.example.echo.exception;

public class EmailJaCadastradoException extends RuntimeException{
    public EmailJaCadastradoException() {
        super("Erro: Email ja cadastrado");
    }
}

package com.example.echo.exception;

public class BicicletaNaoEncontradaException extends RuntimeException{
    public BicicletaNaoEncontradaException(){super("Erro: Bicicleta não encontrada");}

}


package com.example.echo.exception;

public class FuncionarioNaoEncontradoException extends RuntimeException{
    public FuncionarioNaoEncontradoException() {
        super("Erro: Funcionario não encontrado!");
    }
}

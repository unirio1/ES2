package com.example.echo.exception;

public class AluguelNaoPermitidoException extends RuntimeException{
    public AluguelNaoPermitidoException() {super("Erro: Aluguel não permitido.");}
}

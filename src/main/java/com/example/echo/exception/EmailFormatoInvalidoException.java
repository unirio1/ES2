package com.example.echo.exception;

public class EmailFormatoInvalidoException extends RuntimeException{
    public EmailFormatoInvalidoException() {
        super("Erro: Formato de Email inválido.");
    }
}

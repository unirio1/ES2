package com.example.echo.exception;

public class EmailNaoExisteException extends RuntimeException{
    public EmailNaoExisteException() {
        super("Erro: Email não existe.");
    }
}

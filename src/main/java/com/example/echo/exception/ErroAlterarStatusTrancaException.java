package com.example.echo.exception;

public class ErroAlterarStatusTrancaException extends RuntimeException{
    public ErroAlterarStatusTrancaException() {super("Erro: Erro ao alterar status da tranca.");}
}

package com.example.echo.exception;

public class CartaoNaoEncontradoException extends RuntimeException{
    public CartaoNaoEncontradoException() {
        super("Erro: Cartao não encontrado!");
    }
}

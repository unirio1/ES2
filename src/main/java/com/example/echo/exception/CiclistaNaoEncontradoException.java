package com.example.echo.exception;

public class CiclistaNaoEncontradoException extends RuntimeException {
    public CiclistaNaoEncontradoException() {
        super("Erro: Ciclista não encontrado!");
    }
}

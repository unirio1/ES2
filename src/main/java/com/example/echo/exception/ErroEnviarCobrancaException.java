package com.example.echo.exception;

public class ErroEnviarCobrancaException extends RuntimeException{
    public ErroEnviarCobrancaException() {
        super("Erro: Erro no envio de cobranca");
    }
}

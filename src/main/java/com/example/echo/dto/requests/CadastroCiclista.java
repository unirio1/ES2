package com.example.echo.dto.requests;


import com.example.echo.models.Cartao;
import com.example.echo.models.Ciclista;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CadastroCiclista {

    @NotNull(message = "O atributo 'ciclista' não pode ser nulo")
    private Ciclista ciclista;

    @NotNull(message = "O atributo 'cartao' não pode ser nulo")
    private Cartao cartao;
}

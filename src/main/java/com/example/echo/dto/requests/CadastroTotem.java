package com.example.echo.dto.requests;


import com.example.echo.models.Totem;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CadastroTotem {

    @NotNull(message = "O atributo 'Totem' não pode ser nulo")
    private Totem Totem;

}


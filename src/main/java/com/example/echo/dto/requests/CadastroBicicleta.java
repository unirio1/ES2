package com.example.echo.dto.requests;

import com.example.echo.models.Bicicleta;
import com.example.echo.models.Tranca;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CadastroBicicleta {
    @NotNull(message = "O atributo 'Bicicleta' não pode ser nulo")
    private Bicicleta Bicicleta;

}


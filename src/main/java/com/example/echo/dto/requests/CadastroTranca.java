package com.example.echo.dto.requests;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.example.echo.models.Tranca;
import jakarta.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CadastroTranca {

    @NotNull(message = "O atributo 'Tranca' não pode ser nulo")
    private Tranca Tranca;
}


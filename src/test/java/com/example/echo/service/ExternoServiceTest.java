package com.example.echo.service;

import com.example.echo.controller.ExternoController;
import com.example.echo.exception.ErroEnvioEmailException;
import com.example.echo.models.Email;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ExternoServiceTest {

//    @Mock
//    private JavaMailSender javaMailSender;

//    @InjectMocks
//    private ExternoController externoController;

    @Autowired
    @InjectMocks
    private ExternoService externoService;

    @AfterEach
    public void clear(){
        externoService = null;
    }

    @Test
    public void testEnviarEmailDeveDarSucesso() {
        String resultado = externoService.enviarEmail();
        assertEquals("SUCESSO", resultado);
    }

    @Test
    public void testEnviarEmailSucesso() {
        // Configuração do objeto de e-mail
        Email email = new Email("destinatario@example.com", "Assunto", "Mensagem", "remetente@example.com", "destinatario@example.com");

        // Execução do método
        ResponseEntity<Object> responseEntity = externoService.enviarEmail(email);

        // Verificação
        assertEquals(ResponseEntity.ok("enviado com sucesso"), responseEntity);
    }

    @Test
    public void testEnviarEmailFalhar() {
        // Configuração do objeto de e-mail
        Email email = new Email("@ example.com", "Assunto", "Mensagem",
                "remetente@example.com", "destinatario@example.com");

        // Verificação
        assertThrows(ErroEnvioEmailException.class, () -> externoService.enviarEmail(email));
    }

}
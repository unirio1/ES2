package com.example.echo.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class EquipamentoServiceTest {

    @Autowired
    private EquipamentoService equipamentoService;

    @Test
    void testAlteraStatusTranca() {
        String resultado = equipamentoService.alteraStatusTranca();
        assertEquals("SUCESSO", resultado);
    }

    @Test
    void testValidaNumeroBicicleta() {
        String resultado = equipamentoService.validaNumeroBicicleta();
        assertEquals("SUCESSO", resultado);
    }
}
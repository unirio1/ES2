package com.example.echo.service;

import com.example.echo.dto.requests.*;
import com.example.echo.dto.responses.AluguelResponse;
import com.example.echo.dto.responses.DevolucaoResponse;
import com.example.echo.exception.*;
import com.example.echo.models.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CiclistaServiceTest {

    @Mock
    private ExternoService externoService;

    @Mock
    private BicicletaService bicicletaService;

    @Mock
    private TrancaService trancaService;

    @Mock
    private TotemService totemService;

    @Autowired
    @InjectMocks
    private CiclistaService ciclistaService;

    @AfterEach
    public void clear(){
        ciclistaService = null;
    }

    @Test
    public void testOlaMundo(){
        String resultado = ciclistaService.aloMundo();
        assertEquals("Alo, mundo", resultado);
    }

    @Test
    public void testCadastrarBicicleta() {

        Totem totem = new Totem(0,"RJ","teste");

        CadastroTotem cadastraTotem = new CadastroTotem(totem);

        when(totemService.cadastraTotem(cadastraTotem.getTotem())).thenReturn(totem);

        assertNotNull(totemService.cadastraTotem(cadastraTotem.getTotem()));

        Tranca tranca = new Tranca(0, 5, "centro",totem, 1,"2015","caloi","Active","15/02/2015:12:00");

        CadastroTranca cadastroTranca = new CadastroTranca(tranca);

        when(trancaService.cadastrarTranca(cadastroTranca.getTranca())).thenReturn(tranca);

        assertNotNull(trancaService.cadastrarTranca(cadastroTranca.getTranca()));

        Bicicleta bicicleta = new Bicicleta(1, "2015", "caloi", "2015", 2, "Ativa");

        CadastroBicicleta cadastroBicicleta = new CadastroBicicleta(bicicleta);

        when(bicicletaService.cadastrarBicicleta(cadastroBicicleta.getBicicleta())).thenReturn(bicicleta);

        assertNotNull(bicicletaService.cadastrarBicicleta(cadastroBicicleta.getBicicleta()));
    }

//    @Test
//    public void testCadastrarTranca() {
//
//        Totem totem = new Totem(0,"RJ","teste");
//
//        Tranca tranca = new Tranca(1, 5, "centro",totem, 1,"2015","caloi","Active","15/02/2015:12:00");
//
//        CadastroTranca cadastroTranca = new CadastroTranca(tranca);
//
//        assertNotNull(trancaService.cadastrarTranca(cadastroTranca.getTranca()));
//    }
//
//    @Test
//    public void testCadastrarTotem() {
//
//        TotemService totemService = new TotemService();
//
//        Totem totem = new Totem(0,"RJ","teste");
//
//
//        CadastroTotem cadastraTotem = new CadastroTotem(totem);
//
//        assertNotNull(totemService.cadastraTotem(cadastraTotem.getTotem()));
//    }


    @Test
    public void testCadastrarCiclista() {
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "dan@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        assertNotNull(ciclistaService.cadastrarCiclista(cadastroCiclista));
    }

    @Test
    public void testCadastrarCiclistaErroEnviaEmail() {

        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        when(externoService.enviarEmail(email)).thenThrow(new ErroEnvioEmailException());

        assertThrows(ErroEnvioEmailException.class, () -> ciclistaService.cadastrarCiclista(cadastroCiclista));
    }

    @Test
    public void testEncontrarCiclistaPorIdDeveFalhar() {

        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Ciclista ciclista = new Ciclista(1, "Ativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "dan@gmail.com", "test", "123");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        assertThrows(CiclistaNaoEncontradoException.class, () -> ciclistaService.encontrarCiclistaPorId(2));
    }

    @Test
    public void testCadastrarCiclistaEmailExistente(){
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclistaCadastrado = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "123@gmail.com", "test", "123");

        Ciclista ciclistaACadastrar = new Ciclista(2, "Inativo", "Pedro", "19/11/99", "3413213",
                passaporte, "Brasileiro", "123@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclistaCadastrado = new CadastroCiclista(ciclistaCadastrado, cartao);

        CadastroCiclista cadastroCiclistaACadastrar = new CadastroCiclista(ciclistaACadastrar, cartao);

        Email email = new Email(cadastroCiclistaCadastrado.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclistaCadastrado.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclistaCadastrado);

        assertThrows(EmailJaCadastradoException.class, () -> ciclistaService.cadastrarCiclista(cadastroCiclistaACadastrar));
    }

    @Test
    public void testAtualizarCiclista() {
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "7443@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        Ciclista ciclistaAtualizado = new Ciclista(ciclista.getId(),ciclista.getStatus() ,"Pedro", "19/11/99", "3413213",
                passaporte, "Brasileiro", "Pedro@gmail.com", "test", "123");

        ciclistaService.atualizarCiclista(1, ciclistaAtualizado);

        Ciclista retornoCiclista = ciclistaService.encontrarCiclistaPorId(1);
        assertEquals("Pedro", retornoCiclista.getNome());
        assertEquals("19/11/99", retornoCiclista.getNascimento());
        assertEquals("3413213", retornoCiclista.getCpf());
        assertEquals(passaporte, retornoCiclista.getPassaporte());
        assertEquals("Brasileiro", retornoCiclista.getNacionalidade());
        assertEquals("Pedro@gmail.com", retornoCiclista.getEmail());
        assertEquals("test", retornoCiclista.getUrlFotoDocumento());
        assertEquals("123", retornoCiclista.getSenha());
    }

    @Test
    public void testAtualizarCiclistaErroEnviaEmail() {
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "dan@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        Ciclista ciclistaAtualizado = new Ciclista(ciclista.getId(),ciclista.getStatus() ,"Pedro", "19/11/99", "3413213",
                passaporte, "Brasileiro", "@gmail.com", "test", "123");

        Email email2 = new Email(ciclistaAtualizado.getEmail(), "Dados atualizados!", "Seus dados foram atualizados com sucesso!", null, null);

        when(externoService.enviarEmail(email2)).thenThrow(new ErroEnvioEmailException());

        assertThrows(ErroEnvioEmailException.class, () -> ciclistaService.atualizarCiclista(1, ciclistaAtualizado));
    }

    @Test
    public void testExisteEmailCiclistaDeveSerFalso() {
        String email = "9564@gmail.com";
        Boolean resultado = ciclistaService.existeEmailCiclista(email);
        assertFalse(resultado);
    }

    @Test
    public void testExisteEmailCiclistaDeveSerTrue() {
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "dan@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        assertTrue(ciclistaService.existeEmailCiclista(cadastroCiclista.getCiclista().getEmail()));
    }

    @Test
    public void testAtivaEmail() {

        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "Dani@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        ciclistaService.ativarEmail(ciclista.getId());

        assertEquals("Ativo", cadastroCiclista.getCiclista().getStatus());
    }

    @Test
    public void testEncontraMeioDePagamento() {
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "ddd@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        assertNotNull(ciclistaService.encontrarMeioDePagamento(1));
    }

    @Test
    public void testEncontraMeioDePagamentoDeveFalhar() {
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "ddd@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        assertThrows(CartaoNaoEncontradoException.class, () -> ciclistaService.encontrarMeioDePagamento(2));
    }

    @Test
    public void testAtualizaMeioDePagamento() {
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        Ciclista ciclista = new Ciclista(1, "Inativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "luc@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        Cartao cartaoAtualizado = new Cartao(cartao.getIdCiclista(), "PPM", "2434221", "12/26", "253");

        ciclistaService.atualizarMeioDePagamento(1, cartaoAtualizado);

        Cartao retornoCartao = ciclistaService.encontrarMeioDePagamento(1);
        assertEquals("PPM", retornoCartao.getNomeTitular());
        assertEquals("2434221", retornoCartao.getNumero());
        assertEquals("12/26", retornoCartao.getValidade());
        assertEquals("253", retornoCartao.getCvv());
    }

//    @Test
//    public void testAlugarBicicleta(){
//        testCadastrarBicicleta();
//
//        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
//        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
//        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
//
//        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);
//        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);
//
//        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");
//
//        when(externoService.enviarEmail(email)).thenReturn(response);
//        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);
//
//        ciclistaService.cadastrarCiclista(cadastroCiclista);
//        ciclistaService.ativarEmail(1);
//
//        when(externoService.enviarCobranca()).thenReturn("SUCESSO");
//        when(equipamentoService.alteraStatusTranca()).thenReturn("SUCESSO");
//
//        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
//        AluguelResponse aluguelResponse = ciclistaService.realizarAluguel(aluguelRequest);
//        assertNotNull(aluguelResponse);
//    }
//
//    @Test
//    public void testEncontraBicicletaAlugadaPorIdCiclista() {
//        testAlugarBicicleta();
//        Bicicleta bicicleta = ciclistaService.encontraBicicletaAlugadaPorIdCiclista(1);
//        // checar se bicicleta nao é nulo
//        assertNotNull(bicicleta);
//        assertEquals(1, bicicleta.getIdBicicleta());
//        assertEquals("MarcaBike", bicicleta.getMarca());
//        assertEquals("Teste", bicicleta.getModelo());
//        assertEquals("2022", bicicleta.getAno());
//        assertEquals(131, bicicleta.getNumero());
//        assertEquals("Em uso", bicicleta.getStatus());
//    }
//    @Test
//    public void testEncontraCiclistaBicicletaPorIdBicicleta (){
//        testAlugarBicicleta();
//        CiclistaBicicleta ciclistaBicicleta = ciclistaService.encontraCiclistaBicicletaPorIdBicicleta(1);
//        assertNotNull(ciclistaBicicleta);
//
//    }
//
//    @Test
//    public void testeRealizarDevolucao () {
//        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
//        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
//        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
//
//        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);
//
//        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);
//
//        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");
//
//        when(externoService.enviarEmail(email)).thenReturn(response);
//        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);
//
//        ciclistaService.cadastrarCiclista(cadastroCiclista);
//        ciclistaService.ativarEmail(1);
//
//        when(externoService.enviarCobranca()).thenReturn("SUCESSO");
//        when(equipamentoService.alteraStatusTranca()).thenReturn("SUCESSO");
//
//        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
//        AluguelResponse aluguelResponse = ciclistaService.realizarAluguel(aluguelRequest);
//        assertNotNull(aluguelResponse);
//
//        DevolucaoRequest devolucaoRequest = new DevolucaoRequest();
//        devolucaoRequest.setIdTranca(123);
//        devolucaoRequest.setIdBicicleta(aluguelResponse.getBicicleta());
//
//        when(equipamentoService.validaNumeroBicicleta()).thenReturn("SUCESSO");
//
//        DevolucaoResponse devolucaoResponse = ciclistaService.realizarDevolucao(devolucaoRequest);
//
//        assertNotNull(devolucaoResponse);
//        // ver se as infos de bicicleta estao iguais
//        assertEquals(aluguelResponse.getBicicleta(), devolucaoResponse.getBicicleta());
//
//    }

    @Test
    public void testPermiteAluguelCiclistaInativo(){
        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);
        assertFalse(ciclistaService.permiteAluguel(ciclista.getId()));
    }

//    @Test
//    public void testPermiteAluguelCiclistaComBicicleta(){
//        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
//        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
//        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
//
//        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);
//
//        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);
//
//        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");
//
//        when(externoService.enviarEmail(email)).thenReturn(response);
//        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);
//
//        ciclistaService.cadastrarCiclista(cadastroCiclista);
//
//        ciclistaService.ativarEmail(1);
//
//        when(externoService.enviarCobranca()).thenReturn("SUCESSO");
//        when(equipamentoService.alteraStatusTranca()).thenReturn("SUCESSO");
//
//        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
//        ciclistaService.realizarAluguel(aluguelRequest);
//
//        assertFalse(ciclistaService.permiteAluguel(ciclista.getId()));
//
//    }

    @Test
    public void testEncontraBicicletaAlugadaPorIdCiclistaDeveRetornarNulo(){
        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);
        assertNull(ciclistaService.encontraBicicletaAlugadaPorIdCiclista(ciclista.getId()));
    }

//    @Test
//    public void testEncontraCiclistaBicicletaPorIdBicicletaDeveRetornarNulo(){
//        testAlugarBicicleta();
//        assertNull(ciclistaService.encontraCiclistaBicicletaPorIdBicicleta(9));
//    }
//
//    @Test
//    public void testValorExtraCorreto(){
//        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
//        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
//        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
//
//        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);
//
//        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);
//
//        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");
//
//        when(externoService.enviarEmail(email)).thenReturn(response);
//        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);
//
//        ciclistaService.cadastrarCiclista(cadastroCiclista);
//        ciclistaService.ativarEmail(1);
//
//        when(externoService.enviarCobranca()).thenReturn("SUCESSO");
//        when(equipamentoService.alteraStatusTranca()).thenReturn("SUCESSO");
//
//        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
//        AluguelResponse aluguelResponse = ciclistaService.realizarAluguel(aluguelRequest);
//        assertNotNull(aluguelResponse);
//
//        DevolucaoRequest devolucaoRequest = new DevolucaoRequest();
//        devolucaoRequest.setIdTranca(123);
//        devolucaoRequest.setIdBicicleta(aluguelResponse.getBicicleta());
//
//        CiclistaBicicleta ciclistaBicicleta = ciclistaService.encontraCiclistaBicicletaPorIdBicicleta(devolucaoRequest.getIdBicicleta());
//        ciclistaBicicleta.setHoraDevolucao(LocalDateTime.now().plusHours(4));
//
//        long duracaoMinutos = Duration.between(ciclistaBicicleta.getHoraInicio(), ciclistaBicicleta.getHoraDevolucao()).toMinutes();
//        double horas = (double) duracaoMinutos / 60;
//        long minutosExtras = duracaoMinutos - 120;
//
//        long cobrancaExtra = 0;
//
//        if (horas > 2)
//            cobrancaExtra = (Math.floorDiv(minutosExtras, 30) * 5);
//
//        assertEquals(20, cobrancaExtra);
//    }

    @Test
    public void testRealizarAluguelAluguelNaoPermitido(){
        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);

        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);

        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");

        when(externoService.enviarEmail(email)).thenReturn(response);
        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);

        ciclistaService.cadastrarCiclista(cadastroCiclista);

        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
        assertThrows(AluguelNaoPermitidoException.class, () -> ciclistaService.realizarAluguel(aluguelRequest));
    }

//    @Test
//    public void testRealizarAluguelErroEnviarCobranca(){
//        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
//        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
//        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
//
//        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);
//
//        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);
//
//        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");
//
//        when(externoService.enviarEmail(email)).thenReturn(response);
//        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);
//
//        ciclistaService.cadastrarCiclista(cadastroCiclista);
//        ciclistaService.ativarEmail(cadastroCiclista.getCiclista().getId());
//
//        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
//
//        when(externoService.enviarCobranca()).thenReturn("FRACASSO");
//
//        assertThrows(ErroEnviarCobrancaException.class, () -> ciclistaService.realizarAluguel(aluguelRequest));
//    }
//
//    @Test
//    public void testRealizarAluguelErroAlterarStatusTranca(){
//        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
//        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
//        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
//
//        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);
//
//        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);
//
//        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");
//
//        when(externoService.enviarEmail(email)).thenReturn(response);
//        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);
//
//        ciclistaService.cadastrarCiclista(cadastroCiclista);
//        ciclistaService.ativarEmail(cadastroCiclista.getCiclista().getId());
//
//        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
//
//        when(externoService.enviarCobranca()).thenReturn("SUCESSO");
//        when(equipamentoService.alteraStatusTranca()).thenReturn("FRACASSO");
//
//        assertThrows(ErroAlterarStatusTrancaException.class, () -> ciclistaService.realizarAluguel(aluguelRequest));
//    }
//
//    @Test
//    public void testRealizarAluguelErroEnvioEmail(){
//        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
//        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
//        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","@gmail.com", "test", "123");
//
//        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);
//
//        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);
//
//        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");
//
//        when(externoService.enviarEmail(email)).thenReturn(response);
//        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);
//
//        ciclistaService.cadastrarCiclista(cadastroCiclista);
//        ciclistaService.ativarEmail(cadastroCiclista.getCiclista().getId());
//
//        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
//
//        Email email2 = new Email(ciclista.getEmail(), "Bicicleta alugada!", "Você alugou uma bicicleta com sucesso!", null, null);
//
//        when(externoService.enviarCobranca()).thenReturn("SUCESSO");
//        when(equipamentoService.alteraStatusTranca()).thenReturn("SUCESSO");
//        when(externoService.enviarEmail(email2)).thenThrow(new ErroEnvioEmailException());
//
//        assertThrows(ErroEnvioEmailException.class, () -> ciclistaService.realizarAluguel(aluguelRequest));
//    }
//
//    @Test
//    public void testRealizarDevolucaoNumeroBicicletaInvalido(){
//        Passaporte passaporte = new Passaporte( "2198318937" ,"09/25", "Bras11");
//        Cartao cartao = new Cartao( 1, "DPFM", "213213","12/24", "213");
//        Ciclista ciclista = new Ciclista(1,"Inativo","Daniel","19/11/99","3413213", passaporte,"Brasileiro","Luc@gmail.com", "test", "123");
//
//        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, cartao);
//
//        Email email = new Email(cadastroCiclista.getCiclista().getEmail(), "Cadastrado com sucesso!", "Você foi cadastrado com sucesso!", null, null);
//
//        ResponseEntity<Object> response = ResponseEntity.ok("enviado com sucesso");
//
//        when(externoService.enviarEmail(email)).thenReturn(response);
//        when(externoService.validaCartaoDeCredito(cadastroCiclista.getCartao())).thenReturn(cartao);
//
//        ciclistaService.cadastrarCiclista(cadastroCiclista);
//        ciclistaService.ativarEmail(1);
//
//        when(externoService.enviarCobranca()).thenReturn("SUCESSO");
//        when(equipamentoService.alteraStatusTranca()).thenReturn("SUCESSO");
//
//        AluguelRequest aluguelRequest = new AluguelRequest( 1, 123);
//        AluguelResponse aluguelResponse = ciclistaService.realizarAluguel(aluguelRequest);
//
//        DevolucaoRequest devolucaoRequest = new DevolucaoRequest();
//        devolucaoRequest.setIdTranca(123);
//        devolucaoRequest.setIdBicicleta(aluguelResponse.getBicicleta());
//
//        when(equipamentoService.validaNumeroBicicleta()).thenReturn("FRACASSO");
//        assertThrows(NumeroBicicletaInvalidoException.class, () -> ciclistaService.realizarDevolucao(devolucaoRequest));
//    }

}
package com.example.echo.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EchoServiceTest {

    @Test
    void testAloMundo() {
        EchoService echoService = new EchoService();
        String resultado = echoService.aloMundo();
        assertEquals("Alo, mundo", resultado);
    }
}
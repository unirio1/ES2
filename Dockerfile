FROM maven:3.9.0-eclipse-temurin-17-alpine AS build
COPY . .
RUN mvn clean package

FROM openjdk:17-jdk-alpine
COPY --from=build /target/Echo-1.0.jar Echo-1.0.jar


EXPOSE 8080
ENTRYPOINT ["java","-jar","Echo-1.0.jar"]
